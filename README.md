
<!-- README.md is generated from README.Rmd. Please edit that file -->

# collectr

<!-- badges: start -->

[![Latest
Release](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/adl/collectr/-/badges/release.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/adl/collectr/-/releases)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![coverage
report](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/adl/collectr/badges/master/coverage.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/adl/collectr/-/commits/master)
[![pipeline
status](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/adl/collectr/badges/master/pipeline.svg)](https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/adl/collectr/-/commits/master)

<!-- badges: end -->

L’objectif du package `collectr` est de fournir un ensemble de fonctions
permettant :

- de charger des lots de données au format fichier (avec ou sans
  géométrie)
- de consolider ces lots de données
- de les importer dans une base de données PostgreSQL

## Installation

Installer le package `remotes` si besoin :

``` r
install.packages("remotes")
```

Installer le package `collectr` :

``` r
remotes::install_gitlab('dreal-pdl/csd/adl/collectr', host="gitlab-forge.din.developpement-durable.gouv.fr")
```

## Utilisation

### Chargement du package

Charger le package `collectr` dans la session :

``` r
library(collectr)
```

### Formats disponibles

- XLSX
- GPKG (Geopackage)
- SHP (Shapefile)

### Documentation

La documentation du package est consultable sur ce site :
<https://dreal-pdl.gitlab-pages.din.developpement-durable.gouv.fr/csd/adl/collectr/>
