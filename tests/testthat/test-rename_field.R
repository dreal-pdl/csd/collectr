test_that("rename_field works", {
  skip_on_ci()

  # Créer une table dummy dans le schéma collectr de la base production
  create_dummy(database = "production",
               schema = "collectr",
               table = "dummy",
               role = "admin")

  expect_equal(object = rename_field(database = "production", schema = "collectr", table = "dummy", old_field = "id_row__", new_field = "unique_id", role = "admin"),
               expected = TRUE)
})

test_that("rename_field fails", {
  skip_on_ci()

  # Créer une table dummy dans le schéma collectr de la base production
  create_dummy(database = "production",
               schema = "collectr",
               table = "dummy",
               role = "admin")

  # Vérification que la fonction génère bien une erreur sur la table inexistante
  expect_error(object = rename_field(database = "production", schema = "collectr", table = "test", old_field = "id_row__", new_field = "unique_id", role = "admin"),
               regexp = "La table sp\u00e9cifi\u00e9e n\'existe pas dans la base de donn\u00e9es.")

  # Vérification que la fonction génère bien une erreur en cas d'échec du renommage
  expect_equal(object = rename_field(database = "production", schema = "collectr", table = "dummy", old_field = "unique_id", new_field = "unique_id", role = "admin"),
               expected = TRUE)

  # Simulation d'une connexion invalide (NULL ou autre type)
  fake_connexion <- list()  # Une liste vide ne correspond pas à un objet PostgreSQLConnection

  # Mock de la fonction connect_to_db pour retourner une connexion invalide
  mockery::stub(rename_field, "datalibaba::connect_to_db", fake_connexion)

  # Vérification que la fonction génère bien une erreur sur la connexion invalide
  expect_error(
    rename_field(database, schema, table, old_field, new_field, role),
    "La connexion fournie n'est pas une connexion PostgreSQL valide."
  )

})


