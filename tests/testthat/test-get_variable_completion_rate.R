test_that("get_variable_completion_rate works", {
  df <- data.frame(
    var1 = c(1, 2, NA, 4, 5),
    var2 = c(NA, NA, NA, 3, 4)
  )

  expect_equal(object =  get_variable_completion_rate(df, "var1"),
               expected = 80)
})

test_that("get_variable_completion_rate fails", {
  df <- data.frame(
    var1 = c(1, 2, NA, 4, 5),
    var2 = c(NA, NA, NA, 3, 4)
  )

  expect_error(object =  get_variable_completion_rate(df, "var4"),
               regexp = "La variable sp\u00e9cifi\u00e9e n'existe pas dans le dataframe.")
})
