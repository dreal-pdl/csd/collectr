test_that("schema_exists works", {
  skip_on_ci()
  expect_equal(object = schema_exists(database = "si_eau",
                                      schema = "bdtopage",
                                      role = "admin"),
               expected = TRUE)

  expect_equal(object = schema_exists(database = "si_eau",
                                      schema = "test",
                                      role = "admin"),
               expected = FALSE)
})
