% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/create_dummy.R
\name{create_dummy}
\alias{create_dummy}
\title{Crée une table de test dans une base de données PostgreSQL}
\usage{
create_dummy(database, schema, table, role)
}
\arguments{
\item{database}{Nom de la base de données PostgreSQL.}

\item{schema}{Nom du schéma dans lequel créer la table.}

\item{table}{Nom de la table à créer.}

\item{role}{Nom du rôle (utilisateur) pour se connecter à la base de données.}
}
\value{
Un message indiquant si la table a été créée avec succès.
}
\description{
Cette fonction génère une table de test avec des données
fictives dans une base de données PostgreSQL spécifiée.
}
\examples{
\dontrun{
# Créer une table dummy dans le schéma collectr de la base production
create_dummy(database = "production", 
             schema = "collectr", 
             table = "dummy", 
             role = "admin")
}
}
