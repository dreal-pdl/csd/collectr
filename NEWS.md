# collectr 0.1.5

* Ajout de l'argument `role` pour `table_exists()` dans les fonctions suivantes :
  * `check_structure_table()`
  * `import_and_merge_tables()`
  * `insert_into_table()`
  * `modify_column_type()`

# collectr 0.1.4

* Ajout de l'argument `role` pour `table_exists()` dans la fonction `archive_table()`

# collectr 0.1.3

* Remplacement de la librairie `xlsx` par `readxl`
* Ajout de l'option `ecoSQL=FALSE` à l'appel de la fonction `datalibaba::connect_db()`
* Remplacement de `DBI::dbSendQuery` par `DBI::dbSendStatement` dans la fonction `archive_table()` 

# collectr 0.1.2

* Résolution du bug sur la fonction `import_and_merge_tables()`

# collectr 0.1.1

* Remplacement de la fonction `download_and_extract()` par `download_and_extract_wfs()`

# collectr 0.1.0

* Première version documentée
