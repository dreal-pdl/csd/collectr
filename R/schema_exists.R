# WARNING - Generated by {fusen} from dev/flat_database_utils.Rmd: do not edit by hand # nolint: line_length_linter.

#' Vérifie l'existence d'un schéma dans une base spécifique
#' 
#' @description Fonction pour vérifier l'existence d'un schéma dans une 
#' base spécifique
#'
#' @param database Nom de la base de données.
#' @param schema Schéma de la table à vérifier.
#' @param role Rôle de connexion utilisé.
#' 
#' @return TRUE si le schéma existe, sinon FALSE
#'
#' @importFrom datalibaba list_schemas
#'
#' @export
#' @examples
#' schema_exists(database = "si_eau", 
#'              schema = "bdtopage",
#'              role = "admin")
schema_exists <- function(database, schema, role) {
  # Se connecter à la base de données PostgreSQL
  connexion <- datalibaba::connect_to_db(db = database, 
                                         user = role, 
                                         ecoSQL = FALSE)
  
  if (!inherits(connexion, "DBIConnection")) {
    stop("La connexion fournie n\'est pas une connexion DBI valide.")
  }

  schemas <- datalibaba::list_schemas(connexion)
  schema_exists <- schema %in% schemas

  return(schema_exists)
}
